import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable, ViewChild } from '@angular/core';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { IProduct } from 'models/IProduct';
import { ProductService } from 'src/app/services/product.service';
import { StarComponent } from '../star/star.component';


@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent {
  products: Array<IProduct> = [];
  filteredProducts: Array<IProduct> = [];
  deleteError: boolean = false;
  createError: boolean = false;
  errorMessage: string ='';

  // to access / inject form in our class. Using @ViewChild 
  @ViewChild('productForm') private productForm!: NgForm;

  constructor(private ps: ProductService) { }

  ngOnInit(): void {
    this.ps.getProducts().subscribe({
      next: (res: IProduct[]) => {
        this.products = res;
        this.filteredProducts = this.products;
      },
      error: (err: any) => {
        console.error(err);
        this.errorMessage = err.status;
        console.log(this.errorMessage);
      }      
    });
    
    this.filteredProducts = this.products;
  }


  onFilter($event: any): void {
    console.log($event.target.value.toLowerCase());
    let filterString = $event.target.value.toLowerCase();
    if (filterString === "")
      this.filteredProducts = this.products;
    else
      this.filteredProducts = this.products.filter(p => p.title.toLowerCase().includes(filterString));
    /** 
      //Ternary:
      this.filteredProducts = filterString ==== ""
      ? this.products;
      : this.filteredProducts = this.products.filter(p => p.title.toLowerCase().includes(filterString));
     */
  }

  deleteProduct(id: number): void {
    this.ps.deleteProductbyId(id).subscribe({
      next: () => {
        this.ps.getProducts().subscribe({
          next: (res: IProduct[]) => {
            this.products = res;
            this.filteredProducts = this.products;
          },
          error: (err: any) => console.error(err),
        });
        this.filteredProducts = this.products;
        this.deleteError = false;
      },
      error: (err) => {
        console.error(err);
        this.deleteError = true;
      }
    });
  }

  validateProduct(p: IProduct): Boolean {
    if ('title' in p && 'count' in p && p.title !== "")
      return true;
    return false;

  }

  createProduct(): void {
    let newProduct: IProduct = this.productForm.value;
    //one way to perform validation is a custom method on the object, eg:
    // if(this.validateProduct(newProduct)) 
    //another way is to perform validation is a HTML 5 'required'

    if (this.productForm.valid) {
      console.log("<<<<>>>>" + JSON.stringify(this.productForm.value));
      this.ps.createProduct(newProduct).subscribe({
        next: () => {
          this.ps.getProducts().subscribe({
            next: (res: IProduct[]) => {
              this.products = res;
              this.filteredProducts = this.products;
            },
            error: (err) => console.log(err),
          });
          this.filteredProducts = this.products;
        },
        error: (err) => console.log(err)
      });
      this.productForm.resetForm();
      this.createError = false;
    } else {
      console.log("Error in adding data to table!");
      this.createError = true;
    }
  }
  ngOnDestroy(): void { }

}
