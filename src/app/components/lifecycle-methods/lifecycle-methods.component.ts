import { Component, Input, OnChanges, OnInit, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-lifecycle-methods',
  templateUrl: './lifecycle-methods.component.html',
  styleUrls: ['./lifecycle-methods.component.css']
})
export class LifecycleMethodsComponent{

  constructor() {
    console.log('constructor() was called');
  }
  ngOnChanges(): void{
    console.log('ngOnChanges() was called');
  }

  ngOnInit(): void{
    console.log('ngOnInit() was called');
    
  }

  ngOnDestroy(): void{
    console.log('ngOnDestroy() was called');
    
  }

}
