import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './components/app/app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainComponent } from './components/main/main.component';
import { DatabindingComponent } from './components/databinding/databinding.component';
import { DirectivesComponent } from './components/directives/directives.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';

import { GalleryselectorComponent } from './components/galleryselector/galleryselector.component';
import { LifecycleMethodsComponent } from './components/lifecycle-methods/lifecycle-methods.component';
import { StarComponent } from './components/star/star.component';
import { GalleryComponent } from './components/gallery/gallery.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    DatabindingComponent,
    DirectivesComponent,
    AboutComponent,
    HomeComponent,
    GalleryselectorComponent,
    LifecycleMethodsComponent,
    StarComponent,
    GalleryComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      //localhost:4200/home -> load HomeComponent
      { path: 'about', component: AboutComponent },
      { path: 'home', component: HomeComponent },
      { path: '', component: HomeComponent },
      { path: 'directives', component: DirectivesComponent },
      { path: 'databinding', component: DatabindingComponent },
      { path: 'lifecycle-methods', component: LifecycleMethodsComponent },
      { path: 'gallery1', component: GalleryComponent }, 
      { path: '**', redirectTo: 'home' },
      //{path: '**', redirectTo: 'home', pathMatch: 'full'},

      //{path: '**', component: pageNotFoundComponent},
    ]           //,{useHash: true},
    )],
  //exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
