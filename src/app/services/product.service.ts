import { Injectable } from '@angular/core';
import { IProduct } from 'models/IProduct';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  productsUrl=`http://localhost:3000/products`;
  constructor(private httpClient: HttpClient) { }

  getProducts(): Observable<Array<IProduct>>{
    //This gets the JSON with products:  curl -X GET http://localhost:3000/products
    return this.httpClient.get<Array<IProduct>>(this.productsUrl);
  }

  getProductById(id: number): Observable<IProduct> {
    //This gets the JSON with a product:  curl -X GET http://localhost:3000/products/3
    return this.httpClient.get<IProduct> (`${this.productsUrl}/${id}`);
  }

  deleteProductbyId(id: number): Observable<Object>{
    //curl -X DELETE "http://localhost:3000/products/2"
    return this.httpClient.delete<IProduct> (`${this.productsUrl}/${id}`);
  }

  createProduct(product: IProduct): Observable<Object>{
    //curl -X POST "http://localhost:3000/products" + data
    return this.httpClient.post<IProduct> (`${this.productsUrl}`, product);
  }
}





// return [
//   { title: 'Nike', count: 150, price: 2.25, rating: 5 },
//   { title: 'Superga', count: 75, price: 11, rating: 2.5 },
//   { title: 'Balenciaga', count: 35, price: 20, rating: 4 },
//   { title: 'Puma', count: 30, price: 23, rating: 1 },
// ];