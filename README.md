# JREE27Angular

This is my initial Angular Project. See the [Gitlab page](https://java-remote-ee27-course.gitlab.io/jree27angular)!

It is created in JavaRemotee27 lectures following the examples of trainer and developed later in order to test different possibilities like.

## Uses

- routing
- data binding (interpolation, property binding, event binding, two-way binding)
- creating products web page with server (fake Json server), adding, deleting records - directives page
- using templates (directives page)
- *ngFor, *ngIf etc.   
- test lifecycle methods

## Pages 

- home - links to other pages
- about - uses *ngFor for displaying pictures, *ngIf
- directives - server side queries (does not work in Gitlab, should be downloaded to use it). In Gitlab it shows "Server is not running or accessible!" warning (using templates)
- databinding - interpolation, property binding, event binding, two-way binding demo
- lifecycle methods (logging in console only)
- galleries (not implemented yet)

![src/assets/about_23.10.23.png](src/assets/about_23.10.23.png)

![src/assets/databinding_22.10.23.png](src/assets/databinding_22.10.23.png)

![src/assets/directives_page.png](src/assets/directives_page.png)

![src/assets/directives_server_not_running_29.10.23.png](src/assets/directives_server_not_running_29.10.23.png)

## Created by

Katlin Kalde